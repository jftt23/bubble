# Bubble - A Twitter clone for "Entwicklung verteilter Anwendungen"

## Architecture
The architecture of the overall system is based on modern microservice architecture aspects. In its current state it's only a MVP, and therefore might not comply every rule defined by common Microservice manifestos. Further version or later iterations of the system might apply changes on the architecture.

![Architecture](docs/images/architecture.png "Overall Architecture")

## Setup
Run it with docker-compose.

## Name Origin
Bubble refers to "Filter Bubble", as the system is only accessible by THM students, and therefore like-minded users.
