import { Router } from "express";
import { publishRouter } from './api/users';

const router: Router = Router();

router.use('/users', publishRouter);

export default router;
