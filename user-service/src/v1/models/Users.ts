import Joi = require('joi');
import { plainToClass } from 'class-transformer';
import crypto from 'crypto';
import { StringMappingType } from 'typescript';

export class User {
    userId: string
    userTag: string
    userName: string
    email: string
    password: string
    followers: Following[]
    followees: Following[]

    static fromJSON(data: Object): User {
        return plainToClass(User, data);
    }

    static hashPassword(clearPassword: string): string {
        return crypto.createHash('sha256').update(clearPassword).digest('hex');
    }
}

export class Following {
    userId: string
    userTag: string
    userName: string

    static fromJSON(data: Object): Following {
        return plainToClass(Following, data);
    }
}