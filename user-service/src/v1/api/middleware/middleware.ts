import * as dotenv from "dotenv";
import { Request, Response, NextFunction } from "express";
const jwt = require('jsonwebtoken');
dotenv.config();

export const validateUser = (req: Request, res: Response, next: NextFunction) => {
    console.log(req.headers);
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        const bearerToken = req.headers.authorization.split(' ')[1];
        jwt.verify(bearerToken, process.env.JWT_SECRET, (err: any, decodedBearerToken: any) => {
            if (err) {
              res.status(403).end();
              return;
            }
            res.locals.userId = decodedBearerToken.userId;
            res.locals.timestamp = new Date().getTime();
            next();
        });
        return;
    }
    res.status(401).end();
    return;
};
  
