import { Request, Response, Router } from 'express';
import { validateUser } from './middleware/middleware';
import * as dotenv from "dotenv";
import { mutate, query } from "../drivers/graphql/src/graphql";
import * as Queries from '../drivers/graphql/src/queries';
import { Following, User } from '../models/Users';
const jwt = require('jsonwebtoken');

dotenv.config();

const router: Router = Router();

router.use(validateUser);

// Delete User
router.delete('/:userId', async (req: Request, res: Response) => {
    const result = await mutate({
        mutation: Queries.DELETE_USER,
        variables: {id: req.params.userId}
    });
    if(result.deleteUser.nodesDeleted != 0) {
        res.json(result).send();
    } else {
        res.status(404).end();
    }
});

// Get information about specific user
router.get('/:userId', async (req: Request, res: Response) => {
    const result = await query({
        query: Queries.USER,
        variables: {id: req.params.userId}
    });

    if(Object.keys(result).length !== 0) {
        res.json(result.users[0]).send(); 
    } else {
        res.status(404).end();
    }
});

// Get followers of specific user
router.get('/:userId/followers', async (req: Request, res: Response) => {
    const result = await query({
        query: Queries.FOLLOWERS,
        variables: {id: req.params.userId}
    })

    // const followers: Following = Following.fromJSON(result);
    if(Object.keys(result).length !== 0) {
        res.json(result.users[0].followers).send(); 
    } else {
        res.status(404).end();
    }
});

// Get followees of specific user
router.get('/:userId/followees', async (req: Request, res: Response) => {
    const result = await query({
        query: Queries.FOLLOWEES,
        variables: {id: req.params.userId}
    })

    if(Object.keys(result).length !== 0) {
        res.json(result.users[0].followees).send();
    } else {
        res.status(404).end();
    }
});

// Get followers count of specific user
router.get('/:userId/followers/count', async (req: Request, res: Response) => {
    const result = await query({
        query: Queries.FOLLOWERS,
        variables: {id: req.params.userId}
    })

    if(Object.keys(result).length !== 0) {
        res.json({count: result?.users[0]?.followers?.length || 0}).send();
    } else {
        res.status(404).end();
    }
});

// Get followees of specific user
router.get('/:userId/followees/count', async (req: Request, res: Response) => {
    const result = await query({
        query: Queries.FOLLOWEES,
        variables: {id: req.params.userId}
    })

    const followees: Following = Following.fromJSON(result);

    if(Object.keys(result).length !== 0) {
        res.json({count: result?.users[0]?.followers?.length ?? 0}).send();
    } else {
        res.status(404).end();
    }
});

// Follow a user - making an entrance in the user resource
router.post('/:userId/followees', async (req: Request, res: Response) => {
    if(req.params.userId != res.locals.userId) {
        res.status(403).send();
        return;
    }
    
    const result = await mutate({
        mutation: Queries.FOLLOW,
        variables: {id: req.params.userId, followee: req.body.followee}
    })

    res.json(result).send();
});

// Unfollow a user - Delete follower form user resource
router.delete('/:userId/followees/:followeeId', async (req: Request, res: Response) => {
    if(req.params.userId != res.locals.userId) {
        res.status(403).send();
        return;
    }
    const result = await mutate({
        mutation: Queries.UNFOLLOW,
        variables: {id: req.params.userId, followee: req.params.followeeId }
    })

    res.json(result).send();
});

// Catch 405s
router.all('/', (req: Request, res: Response) => {
    res.status(405).send();
});

export { router as publishRouter };
