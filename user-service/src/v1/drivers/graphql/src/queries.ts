import gql from "graphql-tag";

export const USER = gql`
    query user($id: String) {
        users(where: { userId: $id }) {
            userTag
            userName
            partition
            followers {
                userTag
                userName
                userId
            }
            followees {
                userTag
                userName
                userId
            }
        }
    }
`;

export const FOLLOWERS = gql`
    query user($id: String) {
        users(where: { userId: $id }) {
            followers {
                userTag
                userName
                userId
            }
        }
    }
`;

export const FOLLOWEES = gql`
    query user($id: String) {
        users(where: { userId: $id }) {
            followees {
                userTag
                userName
                userId
            }
        }
    }
`;

export const FOLLOW = gql`
    mutation users($id: String, $followee: String!) {
        updateUsers(
            where: { userId: $id }
            connect: { followees: { where: { userId: $followee } } }
        ) {
            users{
                followees {
                    userTag
                    userName
                    userId
                }
            }
        }
    }
`;

export const UNFOLLOW = gql`
    mutation users($id: String, $followee: String!) {
        updateUsers(
            where: { userId: $id }
            disconnect: { followees: { where: { userId: $followee } } }
        ) {
            users{
                followees {
                    userTag
                    userName
                    userId
                }
            }
        }
    }
`;

export const DELETE_USER = gql`
    mutation deleteUser($userId: String) {
        deleteUsers (where: { userId: $userId }) {
            nodesDeleted
        }
    }
`;
