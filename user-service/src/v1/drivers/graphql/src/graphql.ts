import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { onError } from "apollo-link-error";
import { ApolloLink, DocumentNode, split } from "apollo-link";
import { createHttpLink } from "apollo-link-http";
import fetch from 'node-fetch'
import { API_URL } from "./config";

const httpLink = createHttpLink({
    uri: `${API_URL}/graphql`,
    headers: {
        "keep-alive": "true",
    },
    // @ts-ignore
    fetch: fetch
});

const client = new ApolloClient({
    link: ApolloLink.from([
        onError(({ graphQLErrors }) => {
            if (graphQLErrors)
                console.log(graphQLErrors)
        }),
        httpLink,
    ]),
    cache: new InMemoryCache({
        dataIdFromObject: (object: any) => object._id || null,
    }),
});

export async function query(args: { query: DocumentNode; variables: any }) {
    const response = await client.query({
        query: args.query,
        variables: args.variables,
        fetchPolicy: "no-cache",
    });

    if (response.errors && response.errors.length) {
        throw new Error(response.errors[0].message);
    }

    return response.data;
}

export async function mutate(args: { mutation: DocumentNode; variables: any }) {
    const response: any = await client.mutate({
        mutation: args.mutation,
        variables: args.variables,
        fetchPolicy: "no-cache",
    });

    if (response.errors && response.errors.length) {
        throw new Error(response.errors[0].message);
    }

    return response.data;
}
