import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const TweetSchema = Joi.object({
  message: Joi.string().max(280).required(),
  hashtags: Joi.array().items(Joi.string()).optional(),
  references: Joi.array().items(Joi.string().uuid()).optional(),
});

export class Tweet {
  message: string
  hashtags: string[]
  references: string[]
  tweetId: string
  userId: string

  static fromJSON(data: Object): Tweet {
    const refsAndTags = analyzeTweet(data["message"]);
    const hashtags = refsAndTags.hashtags;
    const references = refsAndTags.references;
    data["hashtags"] = hashtags
    data["references"] = references
    return plainToClass(Tweet, data);
  }
}

export const CommentSchema = Joi.object({
  ogTweetId: Joi.string().uuid().required(),
  ogTweetAuthorId: Joi.string().uuid().required(),
  ogCommentId: Joi.string().uuid().optional(),
  ogTweetCommentId: Joi.string().uuid().optional(),
  tweet: Joi.object({
    message: Joi.string().max(280).required(),
    hashtags: Joi.array().items(Joi.string()).optional(),
    references: Joi.array().items(Joi.string().uuid()).optional(),
  }),
});

export class Comment {
  ogTweetId: string
  ogTweetAuthorId: string
  ogCommentId: String
  ogCommentAuthorId: string
  tweet: Tweet

  constructor(ogTweetId: string, ogTweetAuthorId: string, ogCommentId: string, ogCommentAuthorId: string, tweet: Tweet) {
    this.ogTweetId = ogTweetId;
    this.ogTweetAuthorId = ogTweetAuthorId;
    this.ogCommentId = ogCommentId;
    this.ogCommentAuthorId = ogCommentAuthorId;
    this.tweet = tweet;
  }

  static fromJSON(data: Object): Comment {
    return plainToClass(Comment, data);
  }
}

function analyzeTweet(message: string) {
  const REFERENCES_REG_EXP = /@\w+/g;
  const HASHTAG_REG_EXP = /\#\w+/g;
  let matches;
  const references = [];
  const hashtags = [];
  while(matches = REFERENCES_REG_EXP.exec(message)) references.push(...matches)
  while(matches = HASHTAG_REG_EXP.exec(message)) hashtags.push(...matches)
  return {
    references: references.map(ref => ref.substring(1)),
    hashtags: hashtags.map(hashtag => hashtag.substring(1))
  }
}