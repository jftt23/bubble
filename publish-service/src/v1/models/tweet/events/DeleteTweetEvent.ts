import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const DeleteTweetEventSchema = Joi.object({
    eventType: Joi.string().valid("DELETE_TWEET").optional(),
    tweetId: Joi.string().uuid().required(),
});

export class DeleteTweetEvent {
    eventType: string
    tweetId: string

    constructor(eventType: string, tweetId: string) {
        this.eventType = eventType;
        this.tweetId = tweetId;
    }

    static fromJSON(data: Object): DeleteTweetEvent {
        return plainToClass(DeleteTweetEvent, data);
    }
}