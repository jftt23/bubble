import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const DislikeTweetEventSchema = Joi.object({
    eventType: Joi.string().valid("DISLIKE_TWEET").optional(),
    tweetId: Joi.string().uuid().required(),
});

export class DislikeTweetEvent {
    eventType: string
    tweetId: string

    constructor(eventType: string, tweetId: string) {
        this.eventType = eventType;
        this.tweetId = tweetId;
    }

    static fromJSON(data: Object): DislikeTweetEvent {
        return plainToClass(DislikeTweetEvent, data);
    }
}