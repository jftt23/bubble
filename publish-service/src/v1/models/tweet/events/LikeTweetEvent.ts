import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const LikeTweetEventSchema = Joi.object({
    eventType: Joi.string().valid("LIKE_TWEET").optional(),
    tweetId: Joi.string().uuid().required(),
});

export class LikeTweetEvent {
    eventType: string
    tweetId: string

    constructor(eventType: string, tweetId: string) {
        this.eventType = eventType;
        this.tweetId = tweetId;
    }

    static fromJSON(data: Object): LikeTweetEvent {
        return plainToClass(LikeTweetEvent, data);
    }
}