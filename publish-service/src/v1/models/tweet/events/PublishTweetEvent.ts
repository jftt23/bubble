import Joi = require('joi');
import { plainToClass } from 'class-transformer';
import { Tweet } from '../Tweet';

export const PublishTweetEventSchema = Joi.object({
    eventType: Joi.string().valid("PUBLISH_TWEET").optional(),
    message: Joi.string().max(280).required(),
});

export class PublishTweetEvent {
    eventType: string
    tweet: Tweet

    constructor(eventType: string, tweet: Tweet) {
        this.eventType = eventType;
        this.tweet = tweet;
    }

    static fromJSON(data: Object): PublishTweetEvent {
        return plainToClass(PublishTweetEvent, data);
    }
}