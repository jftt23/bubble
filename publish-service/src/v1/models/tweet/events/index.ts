import { PublishTweetEvent, PublishTweetEventSchema } from './PublishTweetEvent';
import { DeleteTweetEvent, DeleteTweetEventSchema } from './DeleteTweetEvent';
import { LikeTweetEvent, LikeTweetEventSchema } from './LikeTweetEvent';
import { DislikeTweetEvent, DislikeTweetEventSchema } from './DislikeTweetEvent';


export enum EventTypes {
    PUBLISH_TWEET,
    DELETE_TWEET,
    LIKE_TWEET,
    DISLIKE_TWEET,
}

const eventTypeFactory = {
    "PUBLISH_TWEET": (content: Object) => PublishTweetEvent.fromJSON(content),
    "DELETE_TWEET": (content: Object) => DeleteTweetEvent.fromJSON(content),
    "LIKE_TWEET": (content: Object) => LikeTweetEvent.fromJSON(content),
    "DISLIKE_TWEET": (content: Object) => DislikeTweetEvent.fromJSON(content),
};

const eventTypeValidator = {
    "PUBLISH_TWEET": (content: Object) => Boolean(PublishTweetEventSchema.validate(content).error),
    "DELETE_TWEET": (content: Object) => Boolean(DeleteTweetEventSchema.validate(content).error),
    "LIKE_TWEET": (content: Object) => Boolean(LikeTweetEventSchema.validate(content).error),
    "DISLIKE_TWEET": (content: Object) => Boolean(DislikeTweetEventSchema.validate(content).error),
};

// @ts-ignore
export const payloadToEventType = (eventType, content) => eventTypeFactory[eventType]?.apply(content) ?? {}
// @ts-ignore
export const isPayloadInvalid = (eventType, content): Boolean => eventTypeValidator[eventType]?.(content) ?? true;