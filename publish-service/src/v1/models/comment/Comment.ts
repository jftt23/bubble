import Joi = require('joi');
import {plainToClass} from 'class-transformer';
import { Tweet } from '../tweet/Tweet';

export const CommentSchema = Joi.object({
    ogTweetId: Joi.string().uuid().required(),
    ogCommentId: Joi.string().uuid().optional(),
    ogAuthorId: Joi.string().uuid().required(),
    comment: Joi.object({
        message: Joi.string().max(280).required(),
        hashtags: Joi.array().items(Joi.string()).optional(),
        references: Joi.array().items(Joi.string().uuid()).optional(),
    }),
});

export class Comment {
    ogTweetId: string
    ogCommentId: string
    ogAuthorId: string
    comment: Tweet

    constructor(ogTweetId: string, ogCommentId: string, ogAuthorId: string, message: string, hashtags: string[] = null, references: string[] = null) {
      this.ogTweetId = ogTweetId
      this.ogCommentId = ogCommentId
      this.ogAuthorId = ogAuthorId
      this.comment = Tweet.fromJSON({message, hashtags,references});
    }
  
    static fromJSON(data: Object): Comment {
      return plainToClass(Comment, data);
    }
}
