import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const LikeCommentEventSchema = Joi.object({
    eventType: Joi.string().valid("LIKE_COMMENT").optional(),
    ogTweetId: Joi.string().uuid().required(),
    ogTweetAuthorId: Joi.string().uuid().required(),
    ogCommentId: Joi.string().uuid().optional(),
    ogCommentAuthorId: Joi.string().uuid().optional(),
});

export class LikeCommentEvent {
    eventType: string
    ogTweetId: string
    ogAuthorId: string
    ogCommentId: string

    constructor(eventType: string, ogTweetId: string, ogAuthorId: string, commentId: string) {
        this.eventType = eventType;
        this.ogTweetId = ogTweetId;
        this.ogAuthorId = ogAuthorId;
        this.ogCommentId = commentId;
    }

    static fromJSON(data: Object): LikeCommentEvent {
        return plainToClass(LikeCommentEvent, data);
    }
}