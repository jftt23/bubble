import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const DislikeCommentEventSchema = Joi.object({
    eventType: Joi.string().valid("DISLIKE_COMMENT").optional(),
    ogTweetId: Joi.string().uuid().required(),
    ogTweetAuthorId: Joi.string().uuid().required(),
    ogCommentId: Joi.string().uuid().optional(),
    ogCommentAuthorId: Joi.string().uuid().optional(),
});

export class DislikeCommentEvent {
    eventType: string
    ogTweetId: string
    ogAuthorId: string
    commentId: string

    constructor(eventType: string, ogTweetId: string, ogAuthorId: string, commentId: string) {
        this.eventType = eventType;
        this.ogTweetId = ogTweetId;
        this.ogAuthorId = ogAuthorId;
        this.commentId = commentId;
    }

    static fromJSON(data: Object): DislikeCommentEvent {
        return plainToClass(DislikeCommentEvent, data);
    }
}