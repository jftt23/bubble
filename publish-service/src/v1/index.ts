import { Router } from "express";
import { publishRouter } from './api/events';

const router: Router = Router();

router.use('/events', publishRouter);

export default router;
