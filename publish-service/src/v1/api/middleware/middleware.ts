import { Request, Response, NextFunction } from 'express';
import { isPayloadInvalid as isTweetInvalid } from '../../models/tweet/events';
import { isPayloadInvalid as isCommentInvalid } from '../../models/comment/events';
import { EventTypes as CommentEvents } from '../../models/comment/events';
import { EventTypes as TweetEvents } from '../../models/tweet/events';
import * as dotenv from 'dotenv';

dotenv.config();
const jwt = require('jsonwebtoken');

export const validateUser = (req: Request, res: Response, next: NextFunction) => {
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        const bearerToken = req.headers.authorization.split(' ')[1];
        jwt.verify(bearerToken, process.env.JWT_SECRET, (err: any, decodedBearerToken: any) => {
            if (err) {
              res.status(403).end();
              return;
            }
            res.locals.userId = decodedBearerToken.userId;
            res.locals.timestamp = new Date().getTime();
            next();
        });
        return;
    }
    res.status(401).end();
    return;
};
  
export const validateEventType = (req: Request, res: Response, next: NextFunction) => {
    const originalUrl = req.originalUrl.split("/")
    const eventType: string = originalUrl[originalUrl.length-1].toUpperCase()
    if (!(Object.keys(CommentEvents).includes(eventType) || Object.keys(TweetEvents).includes(eventType))) {
        res.status(400).end();
        return;
    }
    next();
}

export const validatePayload = (req: Request, res: Response, next: NextFunction) => {
    const originalUrl = req.originalUrl.split("/")
    const eventType: string = originalUrl[originalUrl.length-1].toUpperCase()
    if (Object.keys(TweetEvents).includes(eventType) && isTweetInvalid(eventType, req.body)){
        res.status(422).end();
        return;
    } 
    if (Object.keys(CommentEvents).includes(eventType) && isCommentInvalid(eventType, req.body)) {
        res.status(422).end();
        return;
    }
    next();
};