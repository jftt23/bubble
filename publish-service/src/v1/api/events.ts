import { Request, Response, Router } from 'express';
import { Tweet, Comment } from '../models/tweet/Tweet';
import { validateEventType, validatePayload, validateUser } from './middleware/middleware';
import { v4 as uuidv4 } from 'uuid';
import * as dotenv from "dotenv";
const events = require('events');

dotenv.config();

const FanoutStub = require('../stub/fanoutStub');
const fanoutStub = new FanoutStub(
  process.env.THRIFT_HOST, // || "localhost", 
  process.env.THRIFT_PORT  // || 9090
);

const eventEmitter = new events.EventEmitter();

eventEmitter.on('publish_tweet', (tweet: any) => {
  fanoutStub.publishTweet(tweet);
})

eventEmitter.on('update_tweet', (tweet: any) => {
  fanoutStub.updateTweet(tweet);
})

eventEmitter.on('publish_comment', (comment: any) => {
  fanoutStub.publishComment(comment);
})

eventEmitter.on('update_comment', (comment: any) => {
  fanoutStub.updateComment(comment);
})

const router: Router = Router();

router.use(validateUser);
router.use(validateEventType);
router.use(validatePayload);

// Create tweet
router.post('/publish_tweet', (req: Request, res: Response) => {
  const tweet: Tweet = Tweet.fromJSON(req.body);
  eventEmitter.emit('publish_tweet', {
    eventType: "PUBLISH_TWEET",
    timestamp: res.locals.timestamp,
    userId: res.locals.userId,
    tweet: { 
      tweetId: uuidv4(), 
      userId: res.locals.userId,
      ...tweet
    }
  });
  res.status(201).end();
});

// Update tweet
router.post(['/like_tweet', '/dislike_tweet', '/delete_tweet'], (req: Request, res: Response) => {
  const originalUrl = req.originalUrl.split("/")
  const path = originalUrl[originalUrl.length-1].toUpperCase()
  eventEmitter.emit('update_tweet', {
    eventType: path,
    userId: res.locals.userId, 
    timestamp: res.locals.timestamp,
    tweetId: req.body["tweetId"]
  });
  res.status(201).end();
});


// Create comment
router.post('/publish_comment', (req: Request, res: Response) => {
  const tweet: Tweet = Tweet.fromJSON({userId: res.locals.userId, ...req.body.tweet});
  let comm = req.body;
  comm.tweet = {...tweet};
  let comment: Comment = Comment.fromJSON(comm);

  comment.tweet = {
    tweetId: uuidv4(),
    ...comment.tweet
  }

  eventEmitter.emit('publish_comment', {
    eventType: "PUBLISH_COMMENT",
    userId: res.locals.userId, 
    timestamp: res.locals.timestamp,
    comment: comment
  });

  res.status(201).end();
});

// Update comment
router.post(['/like_comment', '/dislike_comment', '/delete_comment'], (req: Request, res: Response) => {
  const originalUrl = req.originalUrl.split("/")
  const path = originalUrl[originalUrl.length-1].toUpperCase()
  eventEmitter.emit('update_comment', {
    eventType: path,
    userId: res.locals.userId, 
    timestamp: res.locals.timestamp,
    ogTweetId: req.body["ogTweetId"],
    ogTweetAuthorId: req.body["ogTweetAuthorId"],
    ogCommentId: req.body["ogCommentId"],
    ogCommentAuthorId: req.body["ogCommentAuthorId"]
  });
  res.status(201).end();
});

router.all('/tweet', (req: Request, res: Response) => {
  res.status(405).send();
});

export { router as publishRouter };
