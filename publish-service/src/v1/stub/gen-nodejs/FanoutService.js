//
// Autogenerated by Thrift Compiler (0.14.1)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//
"use strict";

var thrift = require('thrift');
var Thrift = thrift.Thrift;
var Q = thrift.Q;
var Int64 = require('node-int64');


var ttypes = require('./FanoutService_types');
//HELPER FUNCTIONS AND STRUCTURES

var FanoutService_publishTweet_args = function(args) {
  this.tweet = null;
  if (args) {
    if (args.tweet !== undefined && args.tweet !== null) {
      this.tweet = new ttypes.PublishTweetEvent_S(args.tweet);
    }
  }
};
FanoutService_publishTweet_args.prototype = {};
FanoutService_publishTweet_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid) {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.tweet = new ttypes.PublishTweetEvent_S();
        this.tweet.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_publishTweet_args.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_publishTweet_args');
  if (this.tweet !== null && this.tweet !== undefined) {
    output.writeFieldBegin('tweet', Thrift.Type.STRUCT, 1);
    this.tweet.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_publishTweet_result = function(args) {
};
FanoutService_publishTweet_result.prototype = {};
FanoutService_publishTweet_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_publishTweet_result.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_publishTweet_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_publishComment_args = function(args) {
  this.comment = null;
  if (args) {
    if (args.comment !== undefined && args.comment !== null) {
      this.comment = new ttypes.PublishCommentEvent_S(args.comment);
    }
  }
};
FanoutService_publishComment_args.prototype = {};
FanoutService_publishComment_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid) {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.comment = new ttypes.PublishCommentEvent_S();
        this.comment.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_publishComment_args.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_publishComment_args');
  if (this.comment !== null && this.comment !== undefined) {
    output.writeFieldBegin('comment', Thrift.Type.STRUCT, 1);
    this.comment.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_publishComment_result = function(args) {
};
FanoutService_publishComment_result.prototype = {};
FanoutService_publishComment_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_publishComment_result.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_publishComment_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_updateTweet_args = function(args) {
  this.tweetEvent = null;
  if (args) {
    if (args.tweetEvent !== undefined && args.tweetEvent !== null) {
      this.tweetEvent = new ttypes.UpdateTweetEvent_S(args.tweetEvent);
    }
  }
};
FanoutService_updateTweet_args.prototype = {};
FanoutService_updateTweet_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid) {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.tweetEvent = new ttypes.UpdateTweetEvent_S();
        this.tweetEvent.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_updateTweet_args.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_updateTweet_args');
  if (this.tweetEvent !== null && this.tweetEvent !== undefined) {
    output.writeFieldBegin('tweetEvent', Thrift.Type.STRUCT, 1);
    this.tweetEvent.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_updateTweet_result = function(args) {
};
FanoutService_updateTweet_result.prototype = {};
FanoutService_updateTweet_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_updateTweet_result.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_updateTweet_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_updateComment_args = function(args) {
  this.commentEvent = null;
  if (args) {
    if (args.commentEvent !== undefined && args.commentEvent !== null) {
      this.commentEvent = new ttypes.UpdateTweetEvent_S(args.commentEvent);
    }
  }
};
FanoutService_updateComment_args.prototype = {};
FanoutService_updateComment_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid) {
      case 1:
      if (ftype == Thrift.Type.STRUCT) {
        this.commentEvent = new ttypes.UpdateTweetEvent_S();
        this.commentEvent.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_updateComment_args.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_updateComment_args');
  if (this.commentEvent !== null && this.commentEvent !== undefined) {
    output.writeFieldBegin('commentEvent', Thrift.Type.STRUCT, 1);
    this.commentEvent.write(output);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutService_updateComment_result = function(args) {
};
FanoutService_updateComment_result.prototype = {};
FanoutService_updateComment_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true) {
    var ret = input.readFieldBegin();
    var ftype = ret.ftype;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

FanoutService_updateComment_result.prototype.write = function(output) {
  output.writeStructBegin('FanoutService_updateComment_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

var FanoutServiceClient = exports.Client = function(output, pClass) {
  this.output = output;
  this.pClass = pClass;
  this._seqid = 0;
  this._reqs = {};
};
FanoutServiceClient.prototype = {};
FanoutServiceClient.prototype.seqid = function() { return this._seqid; };
FanoutServiceClient.prototype.new_seqid = function() { return this._seqid += 1; };

FanoutServiceClient.prototype.publishTweet = function(tweet, callback) {
  this._seqid = this.new_seqid();
  if (callback === undefined) {
    var _defer = Q.defer();
    this._reqs[this.seqid()] = function(error, result) {
      if (error) {
        _defer.reject(error);
      } else {
        _defer.resolve(result);
      }
    };
    this.send_publishTweet(tweet);
    return _defer.promise;
  } else {
    this._reqs[this.seqid()] = callback;
    this.send_publishTweet(tweet);
  }
};

FanoutServiceClient.prototype.send_publishTweet = function(tweet) {
  var output = new this.pClass(this.output);
  var params = {
    tweet: tweet
  };
  var args = new FanoutService_publishTweet_args(params);
  try {
    output.writeMessageBegin('publishTweet', Thrift.MessageType.ONEWAY, this.seqid());
    args.write(output);
    output.writeMessageEnd();
    this.output.flush();
    var callback = this._reqs[this.seqid()] || function() {};
    delete this._reqs[this.seqid()];
    callback(null);
  }
  catch (e) {
    delete this._reqs[this.seqid()];
    if (typeof output.reset === 'function') {
      output.reset();
    }
    throw e;
  }
};

FanoutServiceClient.prototype.publishComment = function(comment, callback) {
  this._seqid = this.new_seqid();
  if (callback === undefined) {
    var _defer = Q.defer();
    this._reqs[this.seqid()] = function(error, result) {
      if (error) {
        _defer.reject(error);
      } else {
        _defer.resolve(result);
      }
    };
    this.send_publishComment(comment);
    return _defer.promise;
  } else {
    this._reqs[this.seqid()] = callback;
    this.send_publishComment(comment);
  }
};

FanoutServiceClient.prototype.send_publishComment = function(comment) {
  var output = new this.pClass(this.output);
  var params = {
    comment: comment
  };
  var args = new FanoutService_publishComment_args(params);
  try {
    output.writeMessageBegin('publishComment', Thrift.MessageType.ONEWAY, this.seqid());
    args.write(output);
    output.writeMessageEnd();
    this.output.flush();
    var callback = this._reqs[this.seqid()] || function() {};
    delete this._reqs[this.seqid()];
    callback(null);
  }
  catch (e) {
    delete this._reqs[this.seqid()];
    if (typeof output.reset === 'function') {
      output.reset();
    }
    throw e;
  }
};

FanoutServiceClient.prototype.updateTweet = function(tweetEvent, callback) {
  this._seqid = this.new_seqid();
  if (callback === undefined) {
    var _defer = Q.defer();
    this._reqs[this.seqid()] = function(error, result) {
      if (error) {
        _defer.reject(error);
      } else {
        _defer.resolve(result);
      }
    };
    this.send_updateTweet(tweetEvent);
    return _defer.promise;
  } else {
    this._reqs[this.seqid()] = callback;
    this.send_updateTweet(tweetEvent);
  }
};

FanoutServiceClient.prototype.send_updateTweet = function(tweetEvent) {
  var output = new this.pClass(this.output);
  var params = {
    tweetEvent: tweetEvent
  };
  var args = new FanoutService_updateTweet_args(params);
  try {
    output.writeMessageBegin('updateTweet', Thrift.MessageType.ONEWAY, this.seqid());
    args.write(output);
    output.writeMessageEnd();
    this.output.flush();
    var callback = this._reqs[this.seqid()] || function() {};
    delete this._reqs[this.seqid()];
    callback(null);
  }
  catch (e) {
    delete this._reqs[this.seqid()];
    if (typeof output.reset === 'function') {
      output.reset();
    }
    throw e;
  }
};

FanoutServiceClient.prototype.updateComment = function(commentEvent, callback) {
  this._seqid = this.new_seqid();
  if (callback === undefined) {
    var _defer = Q.defer();
    this._reqs[this.seqid()] = function(error, result) {
      if (error) {
        _defer.reject(error);
      } else {
        _defer.resolve(result);
      }
    };
    this.send_updateComment(commentEvent);
    return _defer.promise;
  } else {
    this._reqs[this.seqid()] = callback;
    this.send_updateComment(commentEvent);
  }
};

FanoutServiceClient.prototype.send_updateComment = function(commentEvent) {
  var output = new this.pClass(this.output);
  var params = {
    commentEvent: commentEvent
  };
  var args = new FanoutService_updateComment_args(params);
  try {
    output.writeMessageBegin('updateComment', Thrift.MessageType.ONEWAY, this.seqid());
    args.write(output);
    output.writeMessageEnd();
    this.output.flush();
    var callback = this._reqs[this.seqid()] || function() {};
    delete this._reqs[this.seqid()];
    callback(null);
  }
  catch (e) {
    delete this._reqs[this.seqid()];
    if (typeof output.reset === 'function') {
      output.reset();
    }
    throw e;
  }
};
var FanoutServiceProcessor = exports.Processor = function(handler) {
  this._handler = handler;
};
FanoutServiceProcessor.prototype.process = function(input, output) {
  var r = input.readMessageBegin();
  if (this['process_' + r.fname]) {
    return this['process_' + r.fname].call(this, r.rseqid, input, output);
  } else {
    input.skip(Thrift.Type.STRUCT);
    input.readMessageEnd();
    var x = new Thrift.TApplicationException(Thrift.TApplicationExceptionType.UNKNOWN_METHOD, 'Unknown function ' + r.fname);
    output.writeMessageBegin(r.fname, Thrift.MessageType.EXCEPTION, r.rseqid);
    x.write(output);
    output.writeMessageEnd();
    output.flush();
  }
};
FanoutServiceProcessor.prototype.process_publishTweet = function(seqid, input, output) {
  var args = new FanoutService_publishTweet_args();
  args.read(input);
  input.readMessageEnd();
  this._handler.publishTweet(args.tweet);
};
FanoutServiceProcessor.prototype.process_publishComment = function(seqid, input, output) {
  var args = new FanoutService_publishComment_args();
  args.read(input);
  input.readMessageEnd();
  this._handler.publishComment(args.comment);
};
FanoutServiceProcessor.prototype.process_updateTweet = function(seqid, input, output) {
  var args = new FanoutService_updateTweet_args();
  args.read(input);
  input.readMessageEnd();
  this._handler.updateTweet(args.tweetEvent);
};
FanoutServiceProcessor.prototype.process_updateComment = function(seqid, input, output) {
  var args = new FanoutService_updateComment_args();
  args.read(input);
  input.readMessageEnd();
  this._handler.updateComment(args.commentEvent);
};
