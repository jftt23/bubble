const thrift = require('thrift');
const FanoutService = require('./gen-nodejs/FanoutService');
const ttypes = require('./gen-nodejs/FanoutService_types');
const assert = require('assert');

module.exports = class FanoutStub {
    constructor(host, port) {
        this.transport = thrift.TBufferedTransport;
        this.protocol = thrift.TBinaryProtocol;

        this.connection = thrift.createConnection(host, port, {
            transport : this.transport,
            protocol : this.protocol
        });

        this.connection.on('error', function(err) {
            assert(false, err);
        });

        this.client = thrift.createClient(FanoutService, this.connection);
    }
    
    marshallTweet(tweet) {
        let marshalledTweet = new ttypes.Tweet_S();
        marshalledTweet.message = tweet.message;
        marshalledTweet.hashtags = tweet.hashtags;
        marshalledTweet.references = tweet.references;
        marshalledTweet.tweetId = tweet.tweetId;
        marshalledTweet.userId = tweet.userId;
        return marshalledTweet;
    }

    publishTweet(tweetEvent, callback) {
        let marshalledPublishTweetEvent = new ttypes.PublishTweetEvent_S();
        marshalledPublishTweetEvent.eventType = tweetEvent.eventType;
        marshalledPublishTweetEvent.userId = tweetEvent.userId;
        marshalledPublishTweetEvent.timestamp = tweetEvent.timestamp;
        marshalledPublishTweetEvent.tweet = this.marshallTweet(tweetEvent.tweet);

        this.client.publishTweet(marshalledPublishTweetEvent, callback);
    }

    updateTweet(updateTweet, callback) {
        let marshalledUpdateTweetEvent = new ttypes.UpdateTweetEvent_S();
        marshalledUpdateTweetEvent.eventType = updateTweet.eventType;
        marshalledUpdateTweetEvent.userId = updateTweet.userId;
        marshalledUpdateTweetEvent.timestamp = updateTweet.timestamp;
        marshalledUpdateTweetEvent.tweetId = updateTweet.tweetId;

        this.client.updateTweet(marshalledUpdateTweetEvent, callback);
    }

    marshallComment(comment) {
        let marshalledComment = new ttypes.Comment_S();
        let marshalledTweet = this.marshallTweet(comment.tweet);
        marshalledComment.ogTweetId = comment.ogTweetId;
        marshalledComment.ogCommentId = comment.ogCommentId;
        marshalledComment.ogTweetAuthorId = comment.ogTweetAuthorId;
        marshalledComment.ogCommentAuthorId = comment.ogCommentAuthorId;
        marshalledComment.tweet = marshalledTweet;
        return marshalledComment;
    }

    publishComment(commentEvent, callback) {
        let marshalledPublishCommentEvent = new ttypes.PublishCommentEvent_S();
        marshalledPublishCommentEvent.eventType = commentEvent.eventType;
        marshalledPublishCommentEvent.userId = commentEvent.userId;
        marshalledPublishCommentEvent.timestamp = commentEvent.timestamp;
        marshalledPublishCommentEvent.comment = this.marshallComment(commentEvent.comment);

        // function(err, response)
        this.client.publishComment(marshalledPublishCommentEvent, callback);
    }

    updateComment(updateComment, callback) {
        let marshalledUpdateCommentEvent = new ttypes.UpdateCommentEvent_S();
        marshalledUpdateCommentEvent.eventType = updateComment.eventType;
        marshalledUpdateCommentEvent.userId = updateComment.userId;
        marshalledUpdateCommentEvent.timestamp = updateComment.timestamp;
        marshalledUpdateCommentEvent.ogTweetId = updateComment.ogTweetId;
        marshalledUpdateCommentEvent.ogTweetAuthorId = updateComment.ogTweetAuthorId;
        marshalledUpdateCommentEvent.ogCommentId = updateComment.ogCommentId;
        marshalledUpdateCommentEvent.ogCommentAuthorId = updateComment.ogCommentAuthorId;

        this.client.updateComment(marshalledUpdateCommentEvent, callback);
    }
    
    close() {
        this.client.close();
    }
}