import initStub from "./v1";
import { propagateUserInformation } from "./v1/drivers/redis/utils/prepareTweet";

console.log("Propagate user informations");
propagateUserInformation().then(() => console.log("User Informations propagated"));

const stub = initStub()
stub.listen(process.env.THRIFT_PORT || 9090);
