import { skeleton } from "./drivers/thrift/stub";
import { publishTweet, publishTweetUpdate, publishComment } from "./drivers/redis/src/publish";
import { prepareTweet, prepareUpdate, propagateUserInformation } from "./drivers/redis/utils/prepareTweet";
const events = require('events');

const eventEmitter = new events.EventEmitter();

eventEmitter.on('publish_tweet', async (tweet: any) => {
    console.log("Tweet erhalten: ", tweet);
    const [receivers, hashtags] = await prepareTweet(tweet.tweet);
    console.log("Receivers: ", receivers, " Hashtags: ", hashtags);
    await publishTweet(
        receivers,
        hashtags,
        tweet
    );
})

eventEmitter.on('update_tweet', async (update: any) => {
    console.log("Update Tweet erhalten: ", update);
    const receivers = await prepareUpdate(update.userId);
    console.log("Receivers: ", receivers);
    await publishTweetUpdate(
        receivers,
        update
    );
})

eventEmitter.on('publish_comment', async (commentEvent: any) => {
    console.log("comment erhalten ", commentEvent);
    const originalAuthor: string = commentEvent.comment.ogTweetAuthorId;
    const receiversOfogAuthor = await prepareUpdate(originalAuthor);
    const [receiversOfAuthor, hashtags] = await prepareTweet(commentEvent.comment.tweet);
    const receivers = [...receiversOfogAuthor, ...receiversOfAuthor];
    console.log("Receivers: ", receivers);
    await publishComment(
        receivers,
        hashtags,
        { 
            eventType: commentEvent.eventType,
            tweet: commentEvent.comment.tweet,
            ogTweetId: commentEvent.comment.ogTweetId,
            ogCommentId: commentEvent.comment.ogCommentId,
            ogTweetAuthorId: commentEvent.comment.ogTweetAuthorId,
            ogCommentAuthorId: commentEvent.comment.ogCommentAuthorId,
        }
    );
})

eventEmitter.on('update_comment', async (update: any) => {
    console.log("update erhalten ", update)
})

eventEmitter.on('propagate_users', async () => {
    console.log("Propagate user informations");
    await propagateUserInformation();
    console.log("User informations propagated");
})

const init = () => {
    eventEmitter.emit('propagate_users');
    return skeleton(
        (tweet: any) => eventEmitter.emit('publish_tweet', tweet),
        (update: any) => eventEmitter.emit('update_tweet', update),
        (comment: any) => eventEmitter.emit('publish_comment', comment),
        (update: any) => eventEmitter.emit('update_comment', update)
    )
}

export default init;