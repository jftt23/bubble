import { TemplateLiteralTypeSpan } from "typescript";

const thrift = require("thrift");
const FanoutService = require("./gen-nodejs/FanoutService");
const ttypes = require('./gen-nodejs/FanoutService_types');

export const skeleton = (callbackPublishTweet: any, 
      callbackUpdateTweet: any,
      callbackPublishComment: any,
      callbackUpdateComment: any
    ) => thrift.createServer(FanoutService, {
  publishTweet: function(tweet: any, result: any) {
    callbackPublishTweet(tweet)
  },
  updateTweet: function(tweetUpdate: any, result: any) {
    callbackUpdateTweet(tweetUpdate)
  },
  publishComment: function(comment: any, result: any) {
    callbackPublishComment(comment)
  },
  updateComment: function(commentUpdate: any, result: any) {
    callbackUpdateComment(commentUpdate)
  }
});
