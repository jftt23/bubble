struct Tweet_S {
    1: string tweetId,
    2: string message,
    3: optional list<string> hashtags,
    4: optional list<string> references,
    5: string userId,
}

struct PublishTweetEvent_S {
    1: string eventType,
    2: string userId,
    3: i32 timestamp,
    4: Tweet_S tweet,
}

struct UpdateTweetEvent_S {
    1: string eventType,
    2: string userId,
    3: i32 timestamp,
    4: string tweetId,
}

struct Comment_S {
    1: string ogTweetId
    2: optional string ogCommentId
    3: string ogTweetAuthorId
    4: optional string ogCommentAuthorId
    5: Tweet_S tweet
}

struct PublishCommentEvent_S {
    1: string eventType,
    2: string userId,
    3: i32 timestamp,
    4: Comment_S comment
}

struct UpdateCommentEvent_S {
    1: string eventType
    2: string userId,
    3: i32 timestamp,
    4: string ogTweetId
    5: string ogTweetAuthorId
    6: optional string ogCommentId
    7: optional string ogCommentAuthorId
}

service FanoutService {
    oneway void publishTweet(1: PublishTweetEvent_S tweet);
    oneway void publishComment(1: PublishCommentEvent_S comment);
    oneway void updateTweet(1: UpdateTweetEvent_S tweetEvent);
    oneway void updateComment(1: UpdateTweetEvent_S commentEvent);
}