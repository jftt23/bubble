import { query } from "../../graphql/src/graphql";
import * as Queries from '../../graphql/src/queries';
import { insertUserInformation } from "../src/RedisDriver";

export const prepareTweet = async (tweet: any) => {
    let referenceResult = [];
    const followersResult = await query({
        query: Queries.FOLLOWERS_ID,
        variables: {id: tweet.userId}
    });
    

    if(tweet.references) {
        for(const reference of tweet.references) {
            referenceResult.push(await query({
                query: Queries.FOLLOWERS_ID_BY_NAME,
                variables: {tag: reference}
            }))
        }
    }
    console.log("Followers results ", followersResult);
    console.log("Reference results ", referenceResult);

    return [[...referenceResult, ...followersResult.users[0].followers], tweet.tweet?.hashtags ?? []];
}

export const prepareUpdate = async (userId: any) => {
    const followersResult = await query({
        query: Queries.FOLLOWERS_ID,
        variables: {id: userId}
    });

    return followersResult.users[0].followers;
}

export const getPartitions = async () => {
    const userResults = await query({
        query: Queries.USER_PARTITIONS,
        variables: {}
    })
    return userResults;
}

export const propagateUserInformation = async () => {
    const partitions = await getPartitions();
    for(const partition of partitions.users) {
        insertUserInformation(partition.userId, partition.partition);
    }
}