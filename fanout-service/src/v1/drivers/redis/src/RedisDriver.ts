import * as redis from 'redis';
import * as dotenv from 'dotenv';

dotenv.config();

/*
 * A possible implementation of a message queue, based on redis and the underlying list data structure
 * See: https://redislabs.com/ebook/part-2-core-concepts/chapter-6-application-components-in-redis/6-4-task-queues/6-4-1-first-in-first-out-queues/
 */


const clients: Map<number, redis.RedisClient> = new Map();

// TODO: HARDCODED
for (let n = 1; n <= 3; n++) {
    clients.set(n, redis.createClient(6379, `redis-partition-${n}`));
}


export function insertEventIntoQueue(event: any, userId: string, partition: number) {
	const client = clients.get(partition);
    if(client) {
        client.lpush(`events_${userId}`, JSON.stringify(event));
    }
}

export function insertUserInformation(userId: string, partition: number) {
    const client = clients.get(partition);
    if(client) {
        client.lpush(`register_user`, JSON.stringify({userId: userId}));
    }
}