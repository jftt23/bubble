import { insertEventIntoQueue } from "./RedisDriver";


export const publishTweet = (
        receivers: any[],
        hashtags: string[],
        tweet: any
    ) => {
        for(const receiver of receivers) {
            insertEventIntoQueue(tweet, receiver.userId, receiver.partition);
        }
    }

export const publishTweetUpdate = (
        receivers: any[],
        update: any
    ) => {
        for(const receiver of receivers) {
            insertEventIntoQueue(update, receiver.userId, receiver.partition);
        }
    }

export const publishComment = (
        receivers: any[],
        hashtags: string[],
        comment: any
    ) => {
        for(const receiver of receivers) {
            insertEventIntoQueue(comment, receiver.userId, receiver.partition);
        }
    }