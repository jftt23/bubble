import gql from "graphql-tag";

export const FOLLOWERS_ID = gql`
    query user($id: String!) {
        users(where: { userId: $id }) {
            followers {
                userId
                userName
                userTag
                partition
            }
        }
    }
`;

export const FOLLOWERS_ID_BY_NAME = gql`
    query user($tag: String!) {
        users(where: { userTag: $tag }) {
            userId
            userName
            userTag
            partition
        }
    }
`;

export const USER_PARTITIONS = gql`
    query UsersQuery {
        users {
            userId
            partition
        }
    }
`;
