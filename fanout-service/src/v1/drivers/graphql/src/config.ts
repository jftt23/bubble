import * as dotenv from "dotenv";

dotenv.config();

export const API_URL = `http://${process.env.RELATION_SERVICE_HOST || "relation-service"}:${process.env.RELATION_SERVICE_API || "6000"}`;
export const { JWT_KEY } = process.env;
export const DEV_SERVER_PORT = Number(process.env.DEV_SERVER_PORT);
