import { Readable } from "stream";
// import { RedisClient } from "redis";
// const redis = require('redis');
import * as redis from 'redis';
import { promisify } from "util";
import * as dotenv from 'dotenv';

dotenv.config();

/*
 * A possible implementation of a message queue, based on redis and the underlying list data structure
 * See: https://redislabs.com/ebook/part-2-core-concepts/chapter-6-application-components-in-redis/6-4-task-queues/6-4-1-first-in-first-out-queues/
 */

const queues: Map<string, Readable> = new Map();

function setOnError(client: redis.RedisClient) {
	client.on("error", function(error: any) {
		console.error("RedisClient error: ", error);
	});
}

export async function* generateEventQueue(topic: string) {
	// necessary for blocking invocation BRPOP
	const redisClient: redis.RedisClient = redis.createClient(parseInt(process.env.REDIS_PORT), process.env.REDIS_HOST);
	const asyncBRPOP = promisify(redisClient.brpop).bind(redisClient);
	setOnError(redisClient);
	while (true)
		// @ts-ignore
		yield await asyncBRPOP("BRPOP", `${topic}`, "0");
}