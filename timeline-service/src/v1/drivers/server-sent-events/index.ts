import { Request, Response, NextFunction } from 'express';
import { CommentEvent } from '../../models/comment/events';
import { TweetEvent } from '../../models/tweet/events';

export let clients = new Map();

export const eventHandler = function (request: Request, response: Response, next: NextFunction) {
    const clientId = response.locals.userId;
    if(request.params.userId != clientId) {
      response.status(403).send();
      return;
    }

    const headers = {
      'Content-Type': 'text/event-stream',
      'Connection': 'keep-alive',
      'Cache-Control': 'no-cache'
    };
    response.writeHead(200, headers);

    const data = `data: ${JSON.stringify(clients.get(clientId).events)}\n\n`;
  
    response.write(data);

    request.on('close', () => {
      clients.delete(clientId);
    });
}

export async function publishEvent(receiverId: string, event: CommentEvent | TweetEvent) {
    console.log("publish event ", event)
    const client = clients.get(receiverId);
    if(client) {
      client.response.write(`data: ${JSON.stringify(event)}\n\n`)
    } else {
      client.events.pushAll(event);
    }
}
