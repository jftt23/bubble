import { VirtualTree } from "./VirtualTree";
import { CommentEvent } from "../models/comment/events";
import { TweetEvent } from "../models/tweet/events";
import { PublishTweetEvent } from "../models/tweet/events/PublishTweetEvent";
import { LikeTweetEvent } from "../models/tweet/events/LikeTweetEvent";
import { DislikeTweetEvent } from "../models/tweet/events/DislikeTweetEvent";
import { PublishCommentEvent } from "../models/comment/events/PublishCommentEvent";
import { DeleteTweetEvent } from "../models/tweet/events/DeleteTweetEvent";
import { DeleteCommentEvent } from "../models/comment/events/DeleteCommentEvent";
const util = require('util')

const virtualTree: VirtualTree = new VirtualTree();

export function modifyShadowTable(eventType: string, event: CommentEvent | TweetEvent) {
    console.log("ModifyVirtualTree ", eventType)
    switch(eventType) {
        case "PUBLISH_TWEET": { 
            virtualTree.addTweet(event as PublishTweetEvent); 
            console.log(util.inspect(virtualTree, {showHidden: false, depth: null}));
            return;
        }
        case "DELETE_TWEET": {
            virtualTree.deleteTweet(event as DeleteTweetEvent); 
            console.log(util.inspect(virtualTree, {showHidden: false, depth: null}));
            return;
        }
        case "LIKE_TWEET": {
            virtualTree.applyUpdatesTweet(event as LikeTweetEvent); 
            console.log(util.inspect(virtualTree, {showHidden: false, depth: null}));
            return;
        }
        case "DISLIKE_TWEET": {
            virtualTree.applyUpdatesTweet(event as DislikeTweetEvent); 
            console.log(util.inspect(virtualTree, {showHidden: false, depth: null}));
            return;
        }
        case "PUBLISH_COMMENT": {
            console.log("Publish comment, ", event);
            virtualTree.addComment(event as PublishCommentEvent); 
            console.log(util.inspect(virtualTree, {showHidden: false, depth: null}));
            return;
        }
        case "DELETE_COMMENT": {
            console.log("Delete comemnt, ", event);
            virtualTree.deleteComment(event as DeleteCommentEvent); 
            console.log(util.inspect(virtualTree, {showHidden: false, depth: null}));
            return;
        }
        case "LIKE_COMMENT": console.log("Not Implemented Yet"); return;
        case "DISLIKE_COMMENT": console.log("Not Implemented Yet"); return;
    }
}