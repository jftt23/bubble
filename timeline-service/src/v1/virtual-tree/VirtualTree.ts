import { PlainTweet, Tweet, TweetNode } from '../models/tweet/Tweet';
import { PublishTweetEvent } from '../models/tweet/events/PublishTweetEvent';
import { User } from '../models/User';
import { LikeTweetEvent } from '../models/tweet/events/LikeTweetEvent';
import { DislikeTweetEvent } from '../models/tweet/events/DislikeTweetEvent';
import { DeleteTweetEvent } from '../models/tweet/events/DeleteTweetEvent';
import { DeleteCommentEvent } from '../models/comment/events/DeleteCommentEvent';
import { PublishCommentEvent } from '../models/comment/events/PublishCommentEvent';
import { LikeCommentEvent } from '../models/comment/events/LikeCommentEvent';
import { DislikeCommentEvent } from '../models/comment/events/DislikeCommentEvent';
import { CommentEvent } from "../models/comment/events";
import { TweetEvent } from "../models/tweet/events";
const util = require('util')

export class VirtualTree {
    tweets: TweetNode[]
    tweetCounter: number

    constructor() {
        this.tweets = [];
        this.tweetCounter = 0;
    }

    traverseTweets(tweetId: string) {
        for(const tweet of this.tweets) {
            if(tweet.tweetId == tweetId) return tweet;
        }
        return null;
    }

    traverseComments(ogTweetId: string, ogCommentId: string) {
        const ogTweet = this.traverseTweets(ogTweetId);
        let ogComment = null;
        if(!ogCommentId) return ogTweet;
        if(ogTweet && ogCommentId) {
            ogComment = ogTweet.traverseComments(ogCommentId);
        }
        return ogComment;        
    }
    
    deleteTweet(deleteTweetEvent: DeleteTweetEvent) {
        const index = this.tweets.findIndex(tweet => tweet.tweetId == deleteTweetEvent.tweetId);
        this.tweets.splice(index, 1);
    }

    addTweet(publishTweetEvent: PublishTweetEvent) {
        this.tweets.push(new TweetNode(this.tweetCounter++, null, publishTweetEvent.tweet));
    }

    applyUpdatesTweet(event: LikeTweetEvent | DislikeTweetEvent) {
        const foundTweet: TweetNode = this.traverseTweets(event.tweetId);
        // @ts-ignore
        events[event.eventType](event.userId, foundTweet);
    }

    deleteComment(deleteCommentEvent: DeleteCommentEvent) {
        const ogComment: TweetNode = this.traverseComments(deleteCommentEvent.ogTweetId, deleteCommentEvent.commentId);
        const index = ogComment.parent.comments.findIndex(comment => comment.tweetId = ogComment.tweetId);
        ogComment.parent.comments.splice(index, 1);
    }

    addComment(comment: PublishCommentEvent) {
        const ogComment: TweetNode = this.traverseComments(comment.ogTweetId, comment.ogCommentId);
        if(ogComment) {
            ogComment.addComment(comment.tweet);
        }
    }

    applyUpdatesComment(event: LikeCommentEvent | DislikeCommentEvent) {
        const ogComment: TweetNode = this.traverseComments(event.ogTweetId, event.ogTweetId);
        // @ts-ignore
        events[event.eventType](ogComment);
    }

    modifyShadowTable(eventType: string, event: CommentEvent | TweetEvent) {
        switch(eventType) {
            case "PUBLISH_TWEET": { 
                this.addTweet(event as PublishTweetEvent); 
                console.log(util.inspect(this, {showHidden: false, depth: null}));
                return;
            }
            case "DELETE_TWEET": {
                this.deleteTweet(event as DeleteTweetEvent); 
                console.log(util.inspect(this, {showHidden: false, depth: null}));
                return;
            }
            case "LIKE_TWEET": {
                this.applyUpdatesTweet(event as LikeTweetEvent); 
                console.log(util.inspect(this, {showHidden: false, depth: null}));
                return;
            }
            case "DISLIKE_TWEET": {
                this.applyUpdatesTweet(event as DislikeTweetEvent); 
                console.log(util.inspect(this, {showHidden: false, depth: null}));
                return;
            }
            case "PUBLISH_COMMENT": {
                this.addComment(event as PublishCommentEvent); 
                console.log(util.inspect(this, {showHidden: false, depth: null}));
                return;
            }
            case "DELETE_COMMENT": {
                this.deleteComment(event as DeleteCommentEvent); 
                console.log(util.inspect(this, {showHidden: false, depth: null}));
                return;
            }
            // case "LIKE_COMMENT": virtualTree.applyUpdatesComment(event.ogTweetId, event.ogCommentId, event); return;
            // case "DISLIKE_COMMENT": virtualTree.applyUpdatesComment(event.ogTweetId, event.ogCommentId, event); return;
        }
    }
}

const events = {
    "LIKE_TWEET": (user: User, tweet: TweetNode) => {
        const userIndex = tweet.likees.findIndex(u => user.userId == u.userId);
        if(userIndex == -1) {
            tweet.likes = tweet.likes+1;
            tweet.likees.push(user);
        }
    },
    "LIKE_COMMENT": (user: User, tweet: TweetNode) => {
        tweet.likes = tweet.likes+1;
        tweet.likees.push(user);
    },
    "DISLIKE_TWEET": (user: User, tweet: TweetNode) => {
        const userIndex = tweet.likees.findIndex(u => user.userId == u.userId);
        if(userIndex >= 0) {
            tweet.likes = tweet.likes-1;
            tweet.likees.splice(userIndex,1);
        }
    },
    "DISLIKE_COMMENT": (user: User, tweet: Tweet) => {
        const userIndex = tweet.likees.findIndex(u => user.userId == u.userId);
        if(userIndex >= 0) {
            tweet.likes = tweet.likes-1;
            tweet.likees.splice(userIndex,1);
        }
        return tweet;
    }
}