import * as dotenv from 'dotenv';
import { Readable } from "stream";
import { generateEventQueue } from './drivers/redis/RedisDriver';
import { clients, eventHandler, publishEvent } from './drivers/server-sent-events'
import { Router } from 'express';
import { validatePayload } from '../v1/models/payloadToEventType';
import { CommentEvent } from './models/comment/events';
import { TweetEvent } from './models/tweet/events';
import { VirtualTree } from './virtual-tree/VirtualTree';

dotenv.config();

const router: Router = Router();

const queues = [];

const virtualTrees = new Map();

const registrationQueue = Readable.from(generateEventQueue('register_user'));

registrationQueue.on('data', async message => {
    console.log(message);
    const content: Object = JSON.parse(message[1]);
    console.log(content);
    // @ts-ignore
    const userId = content.userId;
    if(userId) {
        queues.push(createUserQueue(userId));
        clients.set(userId, []);
        virtualTrees.set(userId, new VirtualTree());
    }
})

function createUserQueue(userId: string) {
    const queue = Readable.from(generateEventQueue(`events_${userId}`));
    queue.on('data', async message => {
        const content: Object = JSON.parse(message[1]);
        // @ts-ignore
        const eventType: string = content["eventType"];
        console.log("content: ", content, "\n", "eventtype: ", eventType);
        const event: CommentEvent | TweetEvent = validatePayload(eventType, content);
        const vt = virtualTrees.get(userId);
        if(vt) {
            vt.modifyShadowTable(eventType, event);
        }
        // publishEvent(userId, event);
    })
}


router.get('/events/:userId', eventHandler);

export default router;
