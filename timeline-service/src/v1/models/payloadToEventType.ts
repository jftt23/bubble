import { payloadToEventType as payloadToTweetEvent, EventTypes as TweetEventTypes, isPayloadInvalid as isTweetInvalid } from './tweet/events';
import { payloadToEventType as payloadToCommentEvent, EventTypes as CommentEventTypes, isPayloadInvalid as isCommentInvalid } from './comment/events';

export const validatePayload = (et: string, payload: Object) => {
    const eventType: string = et.toUpperCase();
    if (!isTweetInvalid(eventType, payload)){
        const tmp = payloadToTweetEvent(eventType, payload);
        return tmp;
    } 
    if (!isCommentInvalid(eventType, payload)) {
        console.log("comment");
        return payloadToCommentEvent(eventType, payload);
    }
    return null;
}