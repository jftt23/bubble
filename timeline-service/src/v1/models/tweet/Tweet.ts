import Joi = require('joi');
import { plainToClass } from 'class-transformer';
import { User } from "../User";

type Position = number 

export const TweetSchema = Joi.object({
    message: Joi.string().max(280).required(),
    hashtags: Joi.array().items(Joi.string()).optional(),
    references: Joi.array().items(Joi.string().uuid()).optional(),
});

export class PlainTweet {
    message: string
    hashtags: string[]
    references: string[]
    tweetId: string
    userId: string

    static fromJSON(data: Object): PlainTweet {
        // @ts-ignore
        const refsAndTags = analyzeTweet(data["message"]);
        const hashtags = refsAndTags.hashtags;
        const references = refsAndTags.references;
        // @ts-ignore
        data["hashtags"] = hashtags
        // @ts-ignore
        data["references"] = references
        return plainToClass(PlainTweet, data);
    }
}

export class Tweet {
    // TODO: HERE  
    // author: User
    userId: string
    tweetId: string
    ogTweetId: string
    ogCommentId: string
    message: string
    hashtags: string[]
    // TODO: HERE
    // references: User[]
    references: string[]
    likes: number
    likees: User[]

    constructor(plainTweet: PlainTweet) {
      // TODO: HERE
      // this.author = {userId: plainTweet.userId, userTag: ""};
      this.userId = plainTweet.userId;
      this.message = plainTweet.message;
      this.hashtags = plainTweet.hashtags;
      this.references = plainTweet.references;
      this.likees = [];
      this.likes = 0;
    }
}

export class TweetNode {
    position: Position
    commentCounter: number
    // TODO: HERE
    // author: User
    userId: string
    tweetId: string
    message: string
    hashtags: string[]
    // TODO: HERE 
    // references: User[]
    references: string[]
    comments: TweetNode[]
    likes: number
    likees: User[]
    parent: TweetNode

    constructor(position: number, parent: TweetNode, tweet: Tweet) {
        this.commentCounter = 0;
        this.position = position;
        this.parent = parent;
        // TODO: HERE
        // this.author = tweet.userId;
        this.userId = tweet.userId;
        this.tweetId = tweet.tweetId;
        this.message = tweet.message;
        this.likes = 0;
        this.likees = [];
        this.comments = [];
        this.references = tweet.references;
        this.hashtags = tweet.hashtags;
    }

    traverseComments(ogCommentId: string): TweetNode {
        let ogComment = null;
        for(const comment of this.comments) {
            if(comment.tweetId == ogCommentId) return comment;
        }
        if(!ogComment) {
            for(const comment of this.comments) {
                ogComment = comment.traverseComments(ogCommentId);
                if(ogComment) return ogComment;
            }
        }
        return ogComment;
    }

    addComment(tweet: Tweet) {
        this.comments.push(new TweetNode(this.commentCounter++, this, tweet))
    }
}

function analyzeTweet(message: string) {
    const REFERENCES_REG_EXP = /@\w+/g;
    const HASHTAG_REG_EXP = /\#\w+/g;
    let matches;
    const references = [];
    const hashtags = [];
    while(matches = REFERENCES_REG_EXP.exec(message)) references.push(...matches)
    while(matches = HASHTAG_REG_EXP.exec(message)) hashtags.push(...matches)
    return {
        references: references,
        hashtags: hashtags
    }
}