import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const LikeTweetEventSchema = Joi.object({
    eventType: Joi.string().valid("LIKE_TWEET").optional(),
    tweetId: Joi.string().uuid().required(),
    userId: Joi.string().uuid().required(),
    timestamp: Joi.number().required(),
});

export class LikeTweetEvent {
    eventType: string
    userId: string
    timestamp: number
    tweetId: string

    constructor(eventType: string, tweetId: string, userId: string, timestamp: number) {
        this.eventType = eventType;
        this.tweetId = tweetId;
        this.userId = userId;
        this.timestamp = timestamp;
    }

    static fromJSON(data: Object): LikeTweetEvent {
        return plainToClass(LikeTweetEvent, data);
    }
}