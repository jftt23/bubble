import Joi = require('joi');
import { plainToClass } from 'class-transformer';

export const DislikeTweetEventSchema = Joi.object({
    eventType: Joi.string().valid("DISLIKE_TWEET").optional(),
    tweetId: Joi.string().uuid().required(),
});

export class DislikeTweetEvent {
    eventType: string
    userId: string
    timestamp: number
    tweetId: string

    constructor(eventType: string, tweetId: string, userId: string, timestamp: number) {
        this.eventType = eventType;
        this.tweetId = tweetId;
        this.userId = userId;
        this.timestamp = timestamp;
    }

    static fromJSON(data: Object): DislikeTweetEvent {
        return plainToClass(DislikeTweetEvent, data);
    }
}