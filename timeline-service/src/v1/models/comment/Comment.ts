import Joi = require('joi');
import {plainToClass} from 'class-transformer';
import { Tweet } from '../tweet/Tweet';

export const CommentSchema = Joi.object({
  ogTweetId: Joi.string().uuid().required(),
  ogTweetAuthorId: Joi.string().uuid().required(),
  ogCommentId: Joi.string().uuid().optional(),
  ogTweetCommentId: Joi.string().uuid().optional(),
  tweet: Joi.object({
    message: Joi.string().max(280).required(),
    hashtags: Joi.array().items(Joi.string()).optional(),
    references: Joi.array().items(Joi.string().uuid()).optional(),
  }),
});

export class Comment {
  ogTweetId: string
  ogTweetAuthorId: string
  ogCommentId: String
  ogCommentAuthorId: string
  tweet: Tweet

  constructor(ogTweetId: string, ogTweetAuthorId: string, ogCommentId: string, ogCommentAuthorId: string, tweet: Tweet) {
    this.ogTweetId = ogTweetId;
    this.ogTweetAuthorId = ogTweetAuthorId;
    this.ogCommentId = ogCommentId;
    this.ogCommentAuthorId = ogCommentAuthorId;
    this.tweet = tweet;
  }

  static fromJSON(data: Object): Comment {
    return plainToClass(Comment, data);
  }
}