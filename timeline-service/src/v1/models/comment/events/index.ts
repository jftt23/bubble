import { PublishCommentEvent, PublishCommentEventSchema } from './PublishCommentEvent';
import { DeleteCommentEvent, DeleteCommentEventSchema } from './DeleteCommentEvent';
import { LikeCommentEvent, LikeCommentEventSchema } from './LikeCommentEvent';
import { DislikeCommentEvent, DislikeCommentEventSchema } from './DislikeCommentEvent';

export type CommentEvent = DeleteCommentEvent | DislikeCommentEvent | LikeCommentEvent | PublishCommentEvent;

export enum EventTypes {
    PUBLISH_COMMENT,
    DELETE_COMMENT,
    LIKE_COMMENT,
    DISLIKE_COMMENT
}

const eventTypeFactory = {
    "PUBLISH_COMMENT": (content: Object) => PublishCommentEvent.fromJSON(content),
    "DELETE_COMMENT": (content: Object) => DeleteCommentEvent.fromJSON(content),
    "LIKE_COMMENT": (content: Object) => LikeCommentEvent.fromJSON(content),
    "DISLIKE_COMMENT": (content: Object) => DislikeCommentEvent.fromJSON(content),
};

const eventTypeValidator = {
    "PUBLISH_COMMENT": (content: Object) => Boolean(PublishCommentEventSchema.validate(content).error),
    "DELETE_COMMENT": (content: Object) => Boolean(DeleteCommentEventSchema.validate(content).error),
    "LIKE_COMMENT": (content: Object) => Boolean(LikeCommentEventSchema.validate(content).error),
    "DISLIKE_COMMENT": (content: Object) => Boolean(DislikeCommentEventSchema.validate(content).error),
};

// @ts-ignore
export const payloadToEventType = (eventType: string, content: Object) => eventTypeFactory[eventType]?.(content) ?? {}
// @ts-ignore
export const isPayloadInvalid = (eventType: string, content: Object): Boolean => eventTypeValidator[eventType]?.apply(content) ?? true;