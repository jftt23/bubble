import Joi = require('joi');
import { plainToClass } from 'class-transformer';
import { Tweet } from '../../tweet/Tweet';

export const PublishCommentEventSchema = Joi.object({
    eventType: Joi.string().valid("PUBLISH_COMMENT").optional(),
    ogTweetId: Joi.string().uuid().required(),
    ogTweetAuthorId: Joi.string().uuid().required(),
    ogCommentId: Joi.string().uuid().optional(),
    ogCommentAuthorId: Joi.string().uuid().optional(),
    tweet: Joi.object({
        message: Joi.string().max(280).required(),
    }).required(),
});

export class PublishCommentEvent {
    eventType: string
    ogTweetId: string
    ogTweetAuthorId: string
    ogCommentId: string
    ogCommentAuthorId: string
    tweet: Tweet

    static fromJSON(data: Object): PublishCommentEvent {
        return plainToClass(PublishCommentEvent, data);
    }
}