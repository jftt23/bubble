import morgan from 'morgan';
import express from 'express';
import cors from 'cors';
import * as dotenv from 'dotenv';
import router from './v1';
import { validateUser } from './v1/middleware/middleware';

dotenv.config();
const app = express();

const MORGAN_LOGGING_FORMAT: string =
    ':method :url :status length: :res[content-length] in: :response-time ms';

const CORS_SETTINGS = {
  methods: ['GET', 'POST', 'PUT', 'OPTIONS', 'DELETE'],
  allowedHeaders: ['Content-Type', 'Accept', 'Host', 'User-Agent'],
  origin: 'localhost',
};

// Middleware
app.use(express.json());
app.use(morgan(MORGAN_LOGGING_FORMAT));
app.use(cors(CORS_SETTINGS));
app.use(validateUser);

app.use(router);

app.listen(
    9000,
    '0.0.0.0',
    () => console.log(`Started on: localhost:9000`),
);
