import * as server from "./v1/server";
import * as neo4j from "./v1/neo4j";

async function main() {
    console.log("Starting");

    await neo4j.connect();

    await server.start();

    console.log("Started");
}

main();
