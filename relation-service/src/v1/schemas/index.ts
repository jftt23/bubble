import { ApolloServer } from "apollo-server-express";
import { Neo4jGraphQL } from "@neo4j/graphql";
import { OGM } from "@neo4j/graphql-ogm";
import { driver } from "../neo4j";
import { Context } from "../types";
import * as User from "./User";

export const typeDefs = [User.UserSchema];

export const resolvers = {
    ...User.resolvers,
};

export const ogm = new OGM({
    typeDefs,
    driver,
});

export const neoSchema = new Neo4jGraphQL({
    typeDefs,
    resolvers,
});

export const server: ApolloServer = new ApolloServer({
    schema: neoSchema.schema,
    context: ({ req }) => ({ ogm, driver, req } as Context),
});
