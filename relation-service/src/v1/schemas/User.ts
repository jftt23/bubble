import { Context } from "@neo4j/graphql/dist/types";
import { gql } from "apollo-server-express";
import hashPassword from "../utils/hash-password";
import comparePassword from "../utils/compare-password";
import { v4 as uuidv4 } from "uuid";

export const UserSchema = gql`
    type User {
        userId: String!
        userTag: String!
        userName: String!
        email: String!
        password: String!
        partition: Int!

        followees: [User] @relationship(type: "FOLLOWS", direction: OUT)
        followers: [User] @relationship(type: "FOLLOWS", direction: IN)
    }

    type Mutation {
        signUp(email: String!, password: String!, userName: String!, userTag: String!, partition: Int!): String
        signIn(email: String!, password: String!): String
    }
`;


async function signUp(_root: any, args: { 
    email: string, 
    password: string,
    userName: string,
    userTag: string,
    partition: number
}, context: Context) {
    const User = context.ogm.model("User");

    const [existing] = await User.find({
        where: { OR: [{email: args.email}, {userTag: args.userTag}] },
        context: { ...context, adminOverride: true },
    });
    if (existing) {
        return "";
    }

    const hash = await hashPassword(args.password);
    const userId = uuidv4();

    const [user] = (
        await User.create({
            input: [
                {
                    userId: userId,
                    userName: args.userName,
                    userTag: args.userTag,
                    email: args.email,
                    password: hash,
                    partition: args.partition
                },
            ],
        })
    ).users;

    return user.userId;
}

async function signIn(_root: any, args: { email: string; password: string }, context: Context) {
    const User = context.ogm.model("User");

    const [existing] = await User.find({
        where: { email: args.email },
        context: { ...context, adminOverride: true },
    });

    if (!existing) {
        return "";
    }

    const equal = await comparePassword(args.password, existing.password);
    if (equal) {
        return existing.userId;
    } else {
        return "";
    }
}


export const resolvers = {
    Mutation: {
        signIn,
        signUp
    }
}
