// 
// This code is not mine!!! It's based on a neo4j Node Example 
// by the neo4j team
// The overhead for implementing such basic functionality 
// would cost way to much time in the current scenario 
//

import bcrypt from "bcrypt";

function comparePassword(plainText: string, hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
        bcrypt.compare(plainText, hash, (err, result) => {
            if (err) {
                return reject(err);
            }

            return resolve(result);
        });
    });
}

export default comparePassword;
