// 
// This code is not mine!!! It's based on a neo4j Node Example 
// by the neo4j team
// The overhead for implementing such basic functionality 
// would cost way to much time in the current scenario 
//

import bcrypt from "bcrypt";

const saltRounds = 10;

function hashPassword(plainText: string): Promise<string> {
    return new Promise((resolve, reject) => {
        bcrypt.hash(plainText, saltRounds, (err, hash) => {
            if (err) {
                reject(err);
            }

            resolve(hash);
        });
    });
}

export default hashPassword;
