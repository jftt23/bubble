import * as dotenv from "dotenv";

dotenv.config();

export const HTTP_PORT = Number(process.env.HTTP_PORT || 6000);
export const NODE_ENV = process.env.NODE_ENV as string;
export const NEO_USER: string = (process.env.NEO4J_USER || "neo4j") as string;
export const NEO_PASSWORD: string = (process.env.NEO4J_PASSWORD || "password1234") as string;
export const NEO_URL: string = (process.env.NEO4J_URL || "neo4j://neo4j:7687/neo4j") as string;
export const NEO4J_GRAPHQL_JWT_SECRET: string = (process.env.NEO4J_GRAPHQL_JWT_SECRET || "secret") as string; 
