import express from "express";
import * as config from "./config";
import * as graphql from "./schemas";

export const app = express();
graphql.server.applyMiddleware({ app });

export function start(): Promise<void> {
    console.log(`Starting on PORT ${config.HTTP_PORT}`);

    return new Promise((resolve, reject): void => {
        try {
            app.listen(config.HTTP_PORT, () => {
                console.log("Started");

                resolve();
            });
        } catch (error) {
            reject(error);
        }
    });
}
