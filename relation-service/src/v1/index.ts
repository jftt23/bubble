import * as server from "./server";
import * as neo4j from "./neo4j";

async function main() {
    console.log("Starting Server");

    await neo4j.connect();

    await server.start();

    console.log("Started Server");
}

main();