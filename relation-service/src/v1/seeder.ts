/*import faker from "faker";
import { ogm } from "./schemas";
import * as neo4j from "./neo4j";

const User = ogm.model("User");
const Blog = ogm.model("Blog");
const Post = ogm.model("Post");
const Comment = ogm.model("Comment");

const usertag = "admin@admin.com";
const username = "Babofett";
const userid = "3c31a184-879a-48c9-988d-464da810a7e8";

async function main() {
    await neo4j.connect();

    await Promise.all([User].map((m) => m.delete({})));

    const { users } = await User.create({
        input: await Promise.all(
            [
                [userid, usertag, username],
                [faker.internet.uuid(), faker.internet.safe_email(), faker.internet.username()],
                [faker.internet.uuid(), faker.internet.safe_email(), faker.internet.username()],
                [faker.internet.uuid(), faker.internet.safe_email(), faker.internet.username()],
            ].map(async ([uid, utag, uname]) => {
                return {
                    uid,
                    utag,
                    uname
                };
            })
        ),
    });

    users[0].follow(users[1]);
    users[0].follow(users[2]);
    users[1].follow(users[0]);
    users[1].follow(users[2]);
    users[1].follow(users[3]);
    users[2].follow(users[1]);
    users[2].follow(users[3]);
    users[3].follow(users[0]);

    await neo4j.disconnect();
}

main();
*/