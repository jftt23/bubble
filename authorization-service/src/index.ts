import v1 from './v1';
import morgan from 'morgan';
import express from 'express';
import cors from 'cors';
import * as dotenv from 'dotenv';

dotenv.config();
const app = express();

const MORGAN_LOGGING_FORMAT: string =
    ':method :url :status length: :res[content-length] in: :response-time ms';

const CORS_SETTINGS = {
  methods: ['GET', 'POST', 'PUT', 'OPTIONS', 'DELETE'],
  allowedHeaders: ['Content-Type', 'Accept', 'Host', 'User-Agent', 'Authorization'],
  origin: '0.0.0.0',
};

// Middleware
app.use(express.json());
app.use(morgan(MORGAN_LOGGING_FORMAT));
app.use(cors(CORS_SETTINGS));

app.use('/v1', v1);

app.listen(
    parseInt(process.env.AUTHORIZATION_SERVICE_PORT || '8000'),
    process.env.AUTHORIZATION_SERVICE_HOST || '0.0.0.0',
    () => console.log(`Started on: ${process.env.AUTHORIZATION_SERVICE_HOST}:${process.env.AUTHORIZATION_SERVICE_PORT}`),
);
