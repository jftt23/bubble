import gql from "graphql-tag";

export const SIGNIN = gql`
    mutation signIn($email: String!, $password: String!) {
        signIn(email: $email, password: $password) 
    }
`;

export const SIGNUP = gql`
    mutation signUp($email: String!, $password: String!, $userName: String!, $userTag: String!, $partition: Int!) {
        signUp(email: $email, password: $password, userName: $userName, userTag: $userTag, partition: $partition)
    }
`;