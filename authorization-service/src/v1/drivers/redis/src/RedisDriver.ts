import * as redis from 'redis';
/*
 * A possible implementation of a message queue, based on redis and the underlying list data structure
 * See: https://redislabs.com/ebook/part-2-core-concepts/chapter-6-application-components-in-redis/6-4-task-queues/6-4-1-first-in-first-out-queues/
 */

const clients: Map<number, redis.RedisClient> = new Map();

for (let n = 1; n <= parseInt(process.env.NUMBER_PARTITIONS); n++) {
    clients.set(n, redis.createClient(6379, `redis-partition-${n}`));
}

export function insertEventIntoQueue(uid: string, partition: number) {
	const client = clients.get(partition);
    if(client) {
        client.lpush(`register_user`, JSON.stringify({userId: uid}));
    }
}