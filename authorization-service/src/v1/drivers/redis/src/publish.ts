import { insertEventIntoQueue } from "./RedisDriver";

export const registerUser = (
        userId: string,
        partition: number
    ) => insertEventIntoQueue(userId, partition);