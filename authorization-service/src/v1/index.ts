import { Request, Response, Router } from 'express';
import * as dotenv from "dotenv";
import { mutate } from "./drivers/graphql/src/graphql";
import * as Queries from './drivers/graphql/src/queries';
import { registerUser } from './drivers/redis/src/publish';
const jwt = require('jsonwebtoken');

dotenv.config();

const router: Router = Router();

// Signup
router.post('/signup', async (req: Request, res: Response) => {
    const partitionNr = Math.floor(Math.random() * parseInt(process.env.NUMBER_PARTITIONS) + 1);
    const result = await mutate({
        mutation: Queries.SIGNUP,
        variables: {
            userName: req.body.userName,
            userTag: req.body.userTag,
            email: req.body.email,
            password: req.body.password,
            partition: partitionNr
        }
    });
    registerUser(result.signUp, partitionNr);   

    res.json({url: `localhost:900${partitionNr}`, ...result}).send();
});

// Signin
router.post('/signin', async (req: Request, res: Response) => {
    const result = await mutate({
        mutation: Queries.SIGNIN,
        variables: { email: req.body.email, password: req.body.password }
    })

    if(result.signIn === "") {
        res.status(401).end();
        return;
    }

    res.json({
        token: jwt.sign({ userId: result.signIn }, 'shhhhh'),
        userId: result.signIn
    }).send();  
});

// Catch 405s
router.all('/', (req: Request, res: Response) => {
    res.status(405).send();
});

export default router;
